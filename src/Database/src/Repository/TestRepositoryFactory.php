<?php

declare(strict_types=1);

namespace Database\Repository;

use Database\Entity\Test;
use Psr\Container\ContainerInterface;

class TestRepositoryFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $entityManager = $container->get('doctrine.entity_manager.orm_default');
        $metadata = $entityManager->getClassMetadata(Test::class);
        return new TestRepository($entityManager, $metadata);
    }
}
