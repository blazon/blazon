<?php

declare(strict_types=1);

namespace Database\Repository;

use Doctrine\ORM\EntityRepository;

class TestRepository extends EntityRepository
{
    public function findByTestMessage()
    {
        $query = $this->_em->createQuery("SELECT t FROM Blazon\Database\Entity\Test t WHERE t.message LIKE 'This%'");
        return $query->getResult();
    }
}
