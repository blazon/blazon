<?php

declare(strict_types=1);

namespace Database\Command;

use Blazon\Database\Entity\Artist;
use Blazon\Database\Entity\Test;
use Blazon\Database\Repository\TestRepository;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExampleCommand extends Command
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('database:example')
            ->setDescription('Scratch example command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $entityManager = $this->getEntityManager();

        /** @var TestRepository $repo */
        $repo = $entityManager->getRepository(Test::class);

        $result = $repo->findByTestMessage();
//        $result = $repo->findBy(['message' => 'This is a test']);

        print_r($result[0]);
        return 0;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->container->get('doctrine.entity_manager.orm_default');
    }
}
