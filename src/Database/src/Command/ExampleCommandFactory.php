<?php

declare(strict_types=1);

namespace Database\Command;

use Psr\Container\ContainerInterface;

class ExampleCommandFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new ExampleCommand($container);
    }
}
