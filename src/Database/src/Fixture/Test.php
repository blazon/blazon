<?php

declare(strict_types=1);

namespace Database\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Persistence\ObjectManager;

class Test implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $test = new \Blazon\Database\Entity\Test();
        $test->setMessage('This is a test');

        $manager->persist($test);
        $manager->flush();
    }
}
