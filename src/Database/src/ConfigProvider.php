<?php

declare(strict_types=1);

namespace Database;

use Database\Command\ExampleCommand;
use Database\Command\ExampleCommandFactory;
use Database\Repository\TestRepository;
use Database\Repository\TestRepositoryFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\ResolveTargetEntityListener;

class ConfigProvider
{
    public function __invoke() : array
    {
        return [
            'doctrine' => [
                'driver' => [
                    'orm_default' => [
                        'drivers' => ['Database\Entity' => 'database'],
                    ],
                    'database' => [
                        'class' => AnnotationDriver::class,
                        'cache' => 'array',
                        'paths' => __DIR__ . '/Entity',
                    ],
                ],
                'migrations' => [
                    'orm_default' => [
                        'table_storage' => [
                            'table_name' => 'migrations_executed',
                            'version_column_name' => 'version',
                            'version_column_length' => 255,
                            'executed_at_column_name' => 'executed_at',
                            'execution_time_column_name' => 'execution_time',
                        ],
                        'migrations_paths' => ['Database\Migrations' => __DIR__ . '/Migration'],
                        'all_or_nothing' => true,
                        'check_database_platform' => true,
                    ],
                ],
                'fixtures' => [
                    'files' => [],
                    'dirs' => [
                        __DIR__ . '/Fixture',
                    ]
                ],
                'event_manager' => [
                    'orm_default' => [
                        'subscribers' => [ResolveTargetEntityListener::class => ResolveTargetEntityListener::class],
                        'listeners' => [],
                    ],
                ],
                'resolveTargetEntities' => []
            ],
            'console' => [
                'commands' => [
                    ExampleCommand::class => ExampleCommand::class,
                ],
            ],
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    public function getDependencies() : array
    {
        return [
            'invokables' => [],
            'factories'  => [
                TestRepository::class => TestRepositoryFactory::class,
                ExampleCommand::class => ExampleCommandFactory::class,
            ],
        ];
    }

    public function getTemplates() : array
    {
        return [
            'paths' => [
                'database'    => [__DIR__ . '/../templates/'],
            ],
        ];
    }
}
