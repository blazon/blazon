<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit;

use Api\V1\CorsConfigurationLocatorFactory;
use Api\V1\Service\Router;
use Mezzio\Cors\Configuration\ConfigurationInterface;
use Mezzio\Cors\Configuration\RouteConfigurationFactoryInterface;
use Mezzio\Cors\Service\ConfigurationLocator;
use Mezzio\Router\RouterInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;

/** @covers \Api\V1\CorsConfigurationLocatorFactory */
class CorsConfigurationLocatorFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $mockConfig = $this->createMock(ConfigurationInterface::class);
        $mockRequest = $this->createMock(ServerRequestFactoryInterface::class);
        $mockRouter = $this->createMock(RouterInterface::class);
        $mockConfigInterface = $this->createMock(RouteConfigurationFactoryInterface::class);

        $map = [
            [ConfigurationInterface::class, $mockConfig],
            [ServerRequestFactoryInterface::class, $mockRequest],
            [Router::class, $mockRouter],
            [RouteConfigurationFactoryInterface::class, $mockConfigInterface],
        ];

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturnMap($map);

        $provider = new CorsConfigurationLocatorFactory();
        $result = $provider($mockContainer);

        $this->assertInstanceOf(ConfigurationLocator::class, $result);
    }
}
