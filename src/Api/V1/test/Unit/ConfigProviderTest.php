<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit;

use Api\V1\ConfigProvider;
use Mezzio\Hal\Metadata\MetadataMap;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\ConfigProvider */
class ConfigProviderTest extends TestCase
{
    public function testConfigProvider()
    {
        $provider = new ConfigProvider();
        $result = $provider();
        $this->assertIsArray($result);
        $this->assertArrayHasKey('dependencies', $result);
        $this->assertArrayHasKey(MetadataMap::class, $result);
    }
}
