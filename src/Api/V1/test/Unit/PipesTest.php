<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit;

use Api\V1\Pipes;
use Api\V1\Service\CorsMiddleware;
use Api\V1\Service\RouteMiddleware;
use Api\V1\Service\UrlHelperMiddleware;
use Laminas\Stratigility\MiddlewarePipeInterface;
use Mezzio\Middleware\LazyLoadingMiddleware;
use Mezzio\MiddlewareFactory;
use Mezzio\ProblemDetails\ProblemDetailsMiddleware;
use Mezzio\ProblemDetails\ProblemDetailsNotFoundHandler;
use Mezzio\Session\SessionMiddleware;
use Mezzio\Router\Middleware as RouterMiddleware;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Pipes */
class PipesTest extends TestCase
{
    public function testInvoke()
    {
        $requiredPipes = [
            ProblemDetailsMiddleware::class,
            CorsMiddleware::class,
            RouteMiddleware::class,
            RouterMiddleware\ImplicitHeadMiddleware::class,
            RouterMiddleware\ImplicitOptionsMiddleware::class,
            RouterMiddleware\MethodNotAllowedMiddleware::class,
            UrlHelperMiddleware::class,
            RouterMiddleware\DispatchMiddleware::class,
            ProblemDetailsNotFoundHandler::class,
        ];

        $mockFactory = $this->getMockBuilder(MiddlewareFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockPipe = $this->createMock(MiddlewarePipeInterface::class);

        $mockMiddleware = $this->getMockBuilder(LazyLoadingMiddleware::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockPipe->expects($this->any())
            ->method('pipe')
            ->with($this->isInstanceOf(LazyLoadingMiddleware::class));

        $pipes = [];

        $mockFactory->expects($this->any())
            ->method('lazy')
            ->willReturnCallback(function ($name) use (&$pipes, $mockMiddleware) {
                $pipes[] = $name;
                return $mockMiddleware;
            });

        $pipeline = new Pipes();
        $pipeline($mockFactory, $mockPipe);

        foreach ($requiredPipes as $requiredPipe) {
            $this->assertTrue(in_array($requiredPipe, $pipes), 'Missing pipe: ' . $requiredPipe);
        }
    }
}
