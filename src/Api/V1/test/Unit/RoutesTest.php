<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit;

use Api\V1\Routes;
use Mezzio\Middleware\LazyLoadingMiddleware;
use Mezzio\MiddlewareFactory;
use Mezzio\Router\Route;
use Mezzio\Router\RouteCollector;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Routes */
class RoutesTest extends TestCase
{
    private function addFoundRoute(
        array &$foundRoutes,
        string $route,
        array $methods,
        string $name
    ): void {
        if (!isset($foundRoutes[$route])) {
            $foundRoutes[$route] = [
                'methods' => [],
                'name' => ''
            ];
        }

        foreach ($methods as $method) {
            $foundRoutes[$route]['methods'][] = $method;
        }

        $foundRoutes[$route]['name'] = $name;
    }

    public function testInvoke()
    {
        $requiredRoutes = [
            '/ping' => [
                'methods' => ['GET'],
                'name' => 'api.v1.ping'
            ],

            '/users/current' => [
                'methods' => ['GET'],
                'name' => 'api.v1.user.current'
            ],
        ];

        $mockFactory = $this->getMockBuilder(MiddlewareFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockRouteCollector = $this->getMockBuilder(RouteCollector::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockRoute = $this->getMockBuilder(Route::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockLazyMiddleware = $this->getMockBuilder(LazyLoadingMiddleware::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockFactory->expects($this->any())
            ->method('lazy')
            ->willReturn($mockLazyMiddleware);

        $foundRoutes = [];

        $mockRouteCollector->expects($this->any())
            ->method('get')
            ->willReturnCallback(function ($path, $middleware, $name) use (&$foundRoutes, $mockRoute): Route {
                $this->addFoundRoute($foundRoutes, $path, ['GET'], $name);
                return $mockRoute;
            });

        $routes = new Routes();
        $routes($mockFactory, $mockRouteCollector);

        foreach ($requiredRoutes as $requiredRoute => $extra) {
            $this->assertArrayHasKey(
                $requiredRoute,
                $foundRoutes,
                'Did not find GET route: ' . $requiredRoute . ' ' . print_r($foundRoutes, true)
            );

            foreach ($extra['methods'] as $requiredMethod) {
                $this->assertTrue(
                    in_array($requiredMethod, $foundRoutes[$requiredRoute]['methods']),
                    $requiredRoute . ' missing required method: ' . $requiredMethod . ' ' . print_r($foundRoutes, true)
                );
            }

            $this->assertEquals(
                $extra['name'],
                $foundRoutes[$requiredRoute]['name'],
                $requiredRoute . ' missing route name: ' . $extra['name'] . ' ' . print_r($foundRoutes, true)
            );
        }
    }
}
