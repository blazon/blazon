<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit;

use Api\V1\CorsMiddlewareFactory;
use Api\V1\Service\CorsConfigurationLocator;
use Mezzio\Cors\Middleware\CorsMiddleware;
use Mezzio\Cors\Service\ConfigurationLocatorInterface;
use Mezzio\Cors\Service\CorsInterface;
use Mezzio\Cors\Service\ResponseFactoryInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Api\V1\CorsMiddlewareFactory */
class CorsMiddlewareFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $mockCors = $this->createMock(CorsInterface::class);
        $mockResponse = $this->createMock(ResponseFactoryInterface::class);
        $mockConfigInterface = $this->createMock(ConfigurationLocatorInterface::class);

        $map = [
            [CorsInterface::class, $mockCors],
            [CorsConfigurationLocator::class, $mockConfigInterface],
            [ResponseFactoryInterface::class, $mockResponse],
        ];

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturnMap($map);

        $provider = new CorsMiddlewareFactory();
        $result = $provider($mockContainer);

        $this->assertInstanceOf(CorsMiddleware::class, $result);
    }
}
