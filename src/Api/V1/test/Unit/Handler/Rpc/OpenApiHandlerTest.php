<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Handler\Rpc;

use Api\V1\Handler\Rpc\OpenApiHandler;
use Laminas\Diactoros\Response\JsonResponse;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

/** @covers \Api\V1\Handler\Rpc\OpenApiHandler */
class OpenApiHandlerTest extends TestCase
{
    public function testResponse(): void
    {
        $handler = new OpenApiHandler();
        $response    = $handler->handle(
            $this->createMock(ServerRequestInterface::class)
        );

        $this->assertInstanceOf(JsonResponse::class, $response);
    }
}
