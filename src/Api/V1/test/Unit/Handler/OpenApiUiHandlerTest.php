<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Handler;

use Api\V1\Handler\OpenApiUiHandler;
use Laminas\Diactoros\Response\HtmlResponse;
use Mezzio\Router\RouterInterface;
use Mezzio\Template\TemplateRendererInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

/** @covers \Api\V1\Handler\OpenApiUiHandler */
class OpenApiUiHandlerTest extends TestCase
{
    public function testResponse(): void
    {
        $uri = '/some-page';
        $mockHtml = "<some-tag>This is a test</some-tag>";

        $mockRouter = $this->createMock(RouterInterface::class);
        $mockTemplate = $this->createMock(TemplateRendererInterface::class);

        $handler = new OpenApiUiHandler($mockRouter, $mockTemplate);

        $mockRouter->expects($this->once())
            ->method('generateUri')
            ->with($this->equalTo('api.v1.openapi'))
            ->willReturn($uri);

        $data = [
            'openApiRoute' => '/api/v1' . $uri,
            'layout' => false
        ];

        $mockTemplate->expects($this->once())
            ->method('render')
            ->with(
                $this->equalTo('app::swagger'),
                $this->equalTo($data)
            )->willReturn($mockHtml);

        $response    = $handler->handle(
            $this->createMock(ServerRequestInterface::class)
        );

        $this->assertInstanceOf(HtmlResponse::class, $response);
    }
}
