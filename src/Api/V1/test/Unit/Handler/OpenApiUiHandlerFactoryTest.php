<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Handler;

use Api\V1\Handler\OpenApiUiHandler;
use Api\V1\Handler\OpenApiUiHandlerFactory;
use Api\V1\Service\Router;
use Mezzio\Router\RouterInterface;
use Mezzio\Template\TemplateRendererInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Api\V1\Handler\OpenApiUiHandlerFactory */
class OpenApiUiHandlerFactoryTest extends TestCase
{
    public function testResponse(): void
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $mockRouter = $this->createMock(RouterInterface::class);
        $mockTemplate = $this->createMock(TemplateRendererInterface::class);

        $map = [
            [Router::class, $mockRouter],
            [TemplateRendererInterface::class, $mockTemplate],
        ];

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturnMap($map);

        $factory = new OpenApiUiHandlerFactory();
        $result = $factory($mockContainer);

        $this->assertInstanceOf(OpenApiUiHandler::class, $result);
    }
}
