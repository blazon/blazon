<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit;

use Api\V1\Pipes;
use Api\V1\PipelineFactory;
use Api\V1\Routes;
use Api\V1\Service\Router;
use Laminas\Stratigility\MiddlewarePipe;
use Mezzio\MiddlewareFactory;
use Mezzio\Router\RouteCollector;
use Mezzio\Router\RouterInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Api\V1\PipelineFactory */
class PipelineFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $mockRouter = $this->createMock(RouterInterface::class);

        $mockFactory = $this->getMockBuilder(MiddlewareFactory::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockPipes = $this->getMockBuilder(Pipes::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockRoutes = $this->getMockBuilder(Routes::class)
            ->disableOriginalConstructor()
            ->getMock();

        $map = [
            [MiddlewareFactory::class, $mockFactory],
            [Pipes::class, $mockPipes],
            [Router::class, $mockRouter],
            [Routes::class, $mockRoutes],
        ];

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturnMap($map);

        $mockPipes->expects($this->once())
            ->method('__invoke')
            ->with(
                $this->isInstanceOf(MiddlewareFactory::class),
                $this->isInstanceOf(MiddlewarePipe::class),
            );

        $mockRoutes->expects($this->once())
            ->method('__invoke')
            ->with(
                $this->isInstanceOf(MiddlewareFactory::class),
                $this->isInstanceOf(RouteCollector::class),
            );

        $factory = new PipelineFactory();
        $result = $factory($mockContainer);

        $this->assertInstanceOf(MiddlewarePipe::class, $result);
    }
}
