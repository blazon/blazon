<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\Services;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\Services */
class ServicesTest extends TestCase
{
    protected Services $config;

    protected function setUp(): void
    {
        $this->config = new Services();
    }

    public function testInvoke()
    {
        $result = ($this->config)();
        $this->assertArrayHasKey('dependencies', $result);
    }
}
