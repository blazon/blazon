<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\InputFilters;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\InputFilters */
class InputFiltersTest extends TestCase
{
    protected InputFilters $config;

    protected function setUp(): void
    {
        $this->config = new InputFilters();
    }

    public function testInvoke()
    {
        $result = ($this->config)();
        $this->assertArrayHasKey('input_filter_specs', $result);
        $this->assertIsArray($result['input_filter_specs']);
        $this->assertArrayHasKey('input_filters', $result);
        $this->assertArrayHasKey('dependencies', $result);
    }
}
