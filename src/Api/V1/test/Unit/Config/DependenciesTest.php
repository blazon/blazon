<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\Dependencies;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\Dependencies */
class DependenciesTest extends TestCase
{
    protected Dependencies $config;

    protected function setUp(): void
    {
        $this->config = new Dependencies();
    }

    public function testInvoke()
    {
        $result = ($this->config)();

        $this->assertArrayHasKey('dependencies', $result);
    }
}
