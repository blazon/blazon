<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\Repositories;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\Repositories */
class RepositoriesTest extends TestCase
{
    protected Repositories $config;

    protected function setUp(): void
    {
        $this->config = new Repositories();
    }

    public function testInvoke()
    {
        $result = ($this->config)();
        $this->assertArrayHasKey('dependencies', $result);
    }
}
