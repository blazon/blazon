<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\HalMetadataMap;
use Api\V1\Entity\Response\AccountResponse;
use Api\V1\Entity\Response\AccountResponseCollection;
use Api\V1\Entity\Response\FacilityHoursResponse;
use Api\V1\Entity\Response\FacilityHoursResponseCollection;
use Api\V1\Entity\Response\FacilityResponse;
use Api\V1\Entity\Response\FacilityResponseCollection;
use Api\V1\Entity\Response\UserResponse;
use Api\V1\Entity\Response\UserResponseCollection;
use Mezzio\Hal\Metadata\MetadataMap;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\HalMetadataMap */
class HalMetadataMapTest extends TestCase
{
    protected HalMetadataMap $config;

    protected function setUp(): void
    {
        $this->config = new HalMetadataMap();
    }

    public function testInvoke()
    {
        $requitedMaps = [];

        $result = ($this->config)();
        $this->assertArrayHasKey(MetadataMap::class, $result);

        $map = $result[MetadataMap::class];

        foreach ($requitedMaps as $required) {
            $this->assertArrayHasKey($required, $map, "Missing Metamap for: " . $required);
        }
    }
}
