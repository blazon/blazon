<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\Filters;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\Filters */
class FiltersTest extends TestCase
{
    protected Filters $config;

    protected function setUp(): void
    {
        $this->config = new Filters();
    }

    public function testInvoke()
    {
        $result = ($this->config)();
        $this->assertArrayHasKey('filters', $result);

        $filters = $result['filters'];
        $this->assertArrayHasKey('dependencies', $filters);
    }
}
