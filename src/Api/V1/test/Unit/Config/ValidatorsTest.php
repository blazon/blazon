<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\Validators;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\Validators */
class ValidatorsTest extends TestCase
{
    protected Validators $config;

    protected function setUp(): void
    {
        $this->config = new Validators();
    }

    public function testInvoke()
    {
        $result = ($this->config)();
        $this->assertArrayHasKey('validators', $result);
    }
}
