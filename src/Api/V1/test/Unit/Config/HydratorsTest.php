<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\Hydrators;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\Hydrators */
class HydratorsTest extends TestCase
{
    protected Hydrators $config;

    protected function setUp(): void
    {
        $this->config = new Hydrators();
    }

    public function testInvoke()
    {
        $result = ($this->config)();
        $this->assertArrayHasKey('dependencies', $result);
    }
}
