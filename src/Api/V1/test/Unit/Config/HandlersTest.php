<?php

declare(strict_types=1);

namespace Api\V1\Test\Unit\Config;

use Api\V1\Config\Handlers;
use PHPUnit\Framework\TestCase;

/** @covers \Api\V1\Config\Handlers */
class HandlersTest extends TestCase
{
    protected Handlers $config;

    protected function setUp(): void
    {
        $this->config = new Handlers();
    }

    public function testInvoke()
    {
        $result = ($this->config)();
        $this->assertArrayHasKey('dependencies', $result);
    }
}
