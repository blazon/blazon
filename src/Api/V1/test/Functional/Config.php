<?php

declare(strict_types=1);

namespace Api\V1\Test\Functional;

class Config
{
    public function getApiUri(): string
    {
        return 'http://127.0.0.1:8083/api/v1/';
    }
}
