<?php

declare(strict_types=1);

namespace Api\V1\Test\Functional;

use GuzzleHttp\Client;

class TestCase extends \PHPUnit\Framework\TestCase
{
    protected Config $config;

    protected Client $client;

    protected function setUp(): void
    {
        $this->config = new Config();

        $this->client = new Client([
           'base_uri' => $this->config->getApiUri(),
           'timeout'  => 1.0,
        ]);
    }
}
