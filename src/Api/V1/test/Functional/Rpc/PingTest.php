<?php

declare(strict_types=1);

namespace Api\V1\Test\Functional\Rpc;

use Api\V1\Test\Functional\TestCase;
use DateTime;
use DateTimeImmutable;

class PingTest extends TestCase
{
    public function testPing(): void
    {
        $return = $this->client->get('ping');
        $result = json_decode($return->getBody()->getContents(), true);
        $this->assertArrayHasKey('ack', $result);

        $now = new DateTimeImmutable();

        $resultDateTime = new DateTime();
        $resultDateTime->setTimestamp($result['ack']);
        $this->assertInstanceOf(DateTime::class, $resultDateTime);

        $this->assertEquals($now->format('Y-m-d h:m:s'), $resultDateTime->format('Y-m-d h:m:s'));
    }
}
