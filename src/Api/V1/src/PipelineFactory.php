<?php

declare(strict_types=1);

namespace Api\V1;

use Api\V1\Service\Router;
use Laminas\Stratigility\MiddlewarePipe;
use Mezzio\MiddlewareFactory;
use Mezzio\Router\RouteCollector;
use Psr\Container\ContainerInterface;

class PipelineFactory
{
    public function __invoke(ContainerInterface $container): MiddlewarePipe
    {
        /** @var MiddlewareFactory $factory */
        $factory = $container->get(MiddlewareFactory::class);

        $pipeline = new MiddlewarePipe();
        $pipes = $container->get(Pipes::class);
        $pipes($factory, $pipeline);

        $router = $container->get(Router::class); // Retrieve our module-specific router
        $routeCollector = new RouteCollector($router);    // Create a route collector to simplify routing
        $routes = $container->get(Routes::class);
        $routes($factory, $routeCollector);

        return $pipeline;
    }
}
