<?php

declare(strict_types=1);

namespace Api\V1;

use Api\V1\Service\CorsMiddleware;
use Api\V1\Service\RouteMiddleware;
use Api\V1\Service\UrlHelperMiddleware;
use Laminas\Stratigility\MiddlewarePipeInterface;
use Mezzio\MiddlewareFactory;
use Mezzio\ProblemDetails\ProblemDetailsMiddleware;
use Mezzio\ProblemDetails\ProblemDetailsNotFoundHandler;
use Mezzio\Router\Middleware as RouterMiddleware;

class Pipes
{
    public function __invoke(MiddlewareFactory $factory, MiddlewarePipeInterface $pipeline)
    {
        $pipeline->pipe($factory->lazy(CorsMiddleware::class)); // module-specific!
        $pipeline->pipe($factory->lazy(ProblemDetailsMiddleware::class));
        $pipeline->pipe($factory->lazy(RouteMiddleware::class)); // module-specific!
        $pipeline->pipe($factory->lazy(RouterMiddleware\ImplicitHeadMiddleware::class));
        $pipeline->pipe($factory->lazy(RouterMiddleware\ImplicitOptionsMiddleware::class));
        $pipeline->pipe($factory->lazy(RouterMiddleware\MethodNotAllowedMiddleware::class));
        $pipeline->pipe($factory->lazy(UrlHelperMiddleware::class)); // module-specific!
        $pipeline->pipe($factory->lazy(RouterMiddleware\DispatchMiddleware::class));
        $pipeline->pipe($factory->lazy(ProblemDetailsNotFoundHandler::class));
    }
}
