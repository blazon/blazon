<?php

declare(strict_types=1);

namespace Api\V1;

use Api\V1\Handler\OpenApiUiHandler;
use Api\V1\Handler\Rpc\OpenApiHandler;
use Api\V1\Handler\Rpc\PingHandler;
use Mezzio\Authentication\AuthenticationMiddleware;
use Mezzio\Helper\BodyParams\BodyParamsMiddleware;
use Mezzio\MiddlewareFactory;
use Mezzio\Router\RouteCollector;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Routes
{
    public function __invoke(MiddlewareFactory $factory, RouteCollector $route)
    {
        $route->get('/ping', $factory->lazy(PingHandler::class), 'api.v1.ping');
        $route->get('/openapi', $factory->lazy(OpenApiHandler::class), 'api.v1.openapi');
        $route->get('[/]', $factory->lazy(OpenApiUiHandler::class), 'api.v1.openapi.ui');
    }
}
