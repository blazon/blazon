<?php

declare(strict_types=1);

namespace Api\V1;

use Api\V1\Service\CorsConfigurationLocator;
use Mezzio\Cors\Middleware\CorsMiddleware;
use Mezzio\Cors\Service\CorsInterface;
use Mezzio\Cors\Service\ResponseFactoryInterface;
use Psr\Container\ContainerInterface;

class CorsMiddlewareFactory
{
    public function __invoke(ContainerInterface $container): CorsMiddleware
    {
        $cors = $container->get(CorsInterface::class);
        $configurationLocator = $container->get(CorsConfigurationLocator::class);
        $responseFactory = $container->get(ResponseFactoryInterface::class);
        return new CorsMiddleware(
            $cors,
            $configurationLocator,
            $responseFactory
        );
    }
}
