<?php

declare(strict_types=1);

namespace Api\V1;

use Api\V1\Service\Router;
use Mezzio\Cors\Configuration\ConfigurationInterface;
use Mezzio\Cors\Configuration\RouteConfigurationFactoryInterface;
use Mezzio\Cors\Service\ConfigurationLocator;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;

class CorsConfigurationLocatorFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $configuration = $container->get(ConfigurationInterface::class);
        $requestFactory = $container->get(ServerRequestFactoryInterface::class);
        $router = $container->get(Router::class);
        $routeConfigFactory = $container->get(RouteConfigurationFactoryInterface::class);

        return new ConfigurationLocator(
            $configuration,
            $requestFactory,
            $router,
            $routeConfigFactory
        );
    }
}
