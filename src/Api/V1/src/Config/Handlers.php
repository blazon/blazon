<?php

declare(strict_types=1);

namespace Api\V1\Config;

use Api\V1\Handler\OpenApiUiHandler;
use Api\V1\Handler\OpenApiUiHandlerFactory;
use Api\V1\Handler\Rpc as Rpc;
use Api\V1\Handler\Rest as Rest;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Handlers
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                'aliases' => [],

                'invokables' => [
                    Rpc\PingHandler::class => Rpc\PingHandler::class,
                ],

                'factories' => [
                    OpenApiUiHandler::class => OpenApiUiHandlerFactory::class,
                ],
            ],
        ];
    }
}
