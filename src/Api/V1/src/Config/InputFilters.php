<?php

declare(strict_types=1);

namespace Api\V1\Config;

use Api\V1\Config\InputFilters\Facility;
use Api\V1\Config\InputFilters\FacilityHours;
use Api\V1\InputFilter\FacilityCreateInputFilter;
use Api\V1\InputFilter\FacilityCreateInputFilterFactory;
use Api\V1\InputFilter\FacilityHoursInputFilter;
use Laminas\ConfigAggregator\ConfigAggregator;
use Laminas\InputFilter\InputFilterAbstractServiceFactory;

/**
 * Input filter definitions for API Post/Put/Patch requests.
 * This is the preferred method for handling API filtering and field validation.
 *
 * Docs: https://docs.laminas.dev/laminas-inputfilter/specs/#example
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class InputFilters
{
    public function __invoke(): array
    {
        $inputFilters = (new ConfigAggregator([]))->getMergedConfig();

        return [
            'input_filter_specs' => $inputFilters,

            'input_filters' => [
                'abstract_factories' => [
                    InputFilterAbstractServiceFactory::class
                        => InputFilterAbstractServiceFactory::class,
                ],

                'invokeables' => [
                ],

                'factories' => [
                ]
            ],

            'dependencies' => [],
        ];
    }
}
