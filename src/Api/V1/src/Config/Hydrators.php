<?php

declare(strict_types=1);

namespace Api\V1\Config;

use Api\V1\Hydrator\Account as Account;
use Api\V1\Hydrator\Facility as Facility;
use Api\V1\Hydrator\FacilityHours as FacilityHours;
use Api\V1\Hydrator\User as User;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Hydrators
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                'aliases' => [],

                'invokables' => [
                ],

                'factories' => [
                ],
            ],
        ];
    }
}
