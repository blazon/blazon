<?php

declare(strict_types=1);

namespace Api\V1\Config;

/**
 * This is for Dependencies that doesn't fit into any of the other
 * config files.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Dependencies
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                'aliases' => [],
                'invokables' => [],
                'factories' => [],
            ],
        ];
    }
}
