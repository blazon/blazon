<?php

declare(strict_types=1);

namespace Api\V1\Config;

/**
 * Used to register custom Validators.  Use Mezzios built in validators whenever possible
 * Please define and use InputFilters for API endpoints.
 *
 * Docs: https://docs.laminas.dev/laminas-validator/set/
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Validators
{
    public function __invoke(): array
    {
        return [
            'validators' => [
                'aliases' => [],
                'invokables' => [],
                'factories' => [],
            ],
        ];
    }
}
