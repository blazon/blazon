<?php

declare(strict_types=1);

namespace Api\V1\Config;

/**
 * Used to register custom Filters.  Use Mezzios built in filters whenever possible
 * Please define and use InputFilters for API endpoints.
 *
 * Docs: https://docs.laminas.dev/laminas-filter/standard-filters/
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Filters
{
    public function __invoke(): array
    {
        return [
            'filters' => [
                'dependencies' => [
                    'aliases' => [],
                    'invokables' => [],
                    'factories' => [],
                ],
            ],
        ];
    }
}
