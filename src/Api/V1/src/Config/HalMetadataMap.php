<?php

declare(strict_types=1);

namespace Api\V1\Config;

use Api\V1\Entity\Response\AccountResponse;
use Api\V1\Entity\Response\AccountResponseCollection;
use Api\V1\Entity\Response\FacilityHoursResponse;
use Api\V1\Entity\Response\FacilityHoursResponseCollection;
use Api\V1\Entity\Response\FacilityResponse;
use Api\V1\Entity\Response\FacilityResponseCollection;
use Api\V1\Entity\Response\UserResponse;
use Api\V1\Entity\Response\UserResponseCollection;
use Laminas\Hydrator\ReflectionHydrator;
use Mezzio\Hal\Metadata\MetadataMap;
use Mezzio\Hal\Metadata\RouteBasedCollectionMetadata;
use Mezzio\Hal\Metadata\RouteBasedResourceMetadata;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class HalMetadataMap
{
    public function __invoke(): array
    {
        return [
            MetadataMap::class => [
            ],

            'dependencies' => [],
        ];
    }
}
