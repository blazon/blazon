<?php

declare(strict_types=1);

namespace Api\V1\Config;

use Api\V1\CorsConfigurationLocatorFactory;
use Api\V1\CorsMiddlewareFactory;
use Api\V1\PipelineFactory;
use Api\V1\Pipes;
use Api\V1\Routes;
use Api\V1\Service as Service;
use Mezzio\Hal\LinkGenerator\MezzioUrlGeneratorFactory;
use Mezzio\Hal\LinkGeneratorFactory;
use Mezzio\Hal\ResourceGeneratorFactory;
use Mezzio\Helper\UrlHelperFactory;
use Mezzio\Helper\UrlHelperMiddlewareFactory;
use Mezzio\Router\FastRouteRouterFactory;
use Mezzio\Router\Middleware\RouteMiddlewareFactory;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Services
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                'aliases' => [
                    Service\EntityManager::class => 'doctrine.entity_manager.orm_default',
                ],

                'invokables' => [
                    Routes::class => Routes::class,
                    Pipes::class => Pipes::class,
                ],

                'factories' => [
                    /* Module Pipelines and Helpers */
                    Service\Pipeline::class => PipelineFactory::class,
                    Service\CorsConfigurationLocator::class => CorsConfigurationLocatorFactory::class,
                    Service\CorsMiddleware::class => CorsMiddlewareFactory::class,
                    Service\Router::class => FastRouteRouterFactory::class,

                    Service\LinkGenerator::class
                        => new LinkGeneratorFactory(Service\UrlGenerator::class),

                    Service\ResourceGenerator::class
                        => new ResourceGeneratorFactory(Service\LinkGenerator::class),

                    Service\RouteMiddleware::class
                        => new RouteMiddlewareFactory(Service\Router::class),

                    Service\UrlHelper::class
                        => new UrlHelperFactory('/api/v1', Service\Router::class),

                    Service\UrlHelperMiddleware::class
                        => new UrlHelperMiddlewareFactory(Service\UrlHelper::class),

                    Service\UrlGenerator::class
                        => new MezzioUrlGeneratorFactory(Service\UrlHelper::class),
                ],
            ],
        ];
    }
}
