<?php

declare(strict_types=1);

namespace Api\V1\Config;

use Api\V1\Repository\AccountRepository;
use Api\V1\Repository\AccountRepositoryFactory;
use Api\V1\Repository\FacilityRepository;
use Api\V1\Repository\FacilityRepositoryFactory;
use Api\V1\Repository\FacilityHoursRepository;
use Api\V1\Repository\FacilityHoursRepositoryFactory;
use Api\V1\Repository\UserRepository;
use Api\V1\Repository\UserRepositoryFactory;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
 */
class Repositories
{
    public function __invoke(): array
    {
        return [
            'dependencies' => [
                'aliases' => [],

                'invokables' => [],

                'factories' => [
                ],
            ],
        ];
    }
}
