<?php

declare(strict_types=1);

namespace Api\V1\Handler;

use Laminas\Diactoros\Response\HtmlResponse;
use Mezzio\Router\RouterInterface;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class OpenApiUiHandler implements RequestHandlerInterface
{
    private RouterInterface $router;
    private TemplateRendererInterface $template;

    public function __construct(
        RouterInterface $router,
        TemplateRendererInterface $template
    ) {
        $this->router        = $router;
        $this->template      = $template;
    }

    /**  @SuppressWarnings(PHPMD.UnusedFormalParameter) */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = [];
        $data['openApiRoute'] = '/api/v1' . $this->router->generateUri('api.v1.openapi');
        $data['layout'] = false;
        return new HtmlResponse($this->template->render('app::swagger', $data));
    }
}
