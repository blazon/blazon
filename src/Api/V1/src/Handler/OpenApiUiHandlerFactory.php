<?php

declare(strict_types=1);

namespace Api\V1\Handler;

use Api\V1\Service\Router;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function get_class;

class OpenApiUiHandlerFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        $router   = $container->get(Router::class);
        $template = $container->get(TemplateRendererInterface::class);

        return new OpenApiUiHandler($router, $template);
    }
}
