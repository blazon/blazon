<?php

declare(strict_types=1);

namespace Api\V1\Handler\Rpc;

use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function OpenApi\scan;

class OpenApiHandler implements RequestHandlerInterface
{
    /**  @SuppressWarnings(PHPMD.UnusedFormalParameter) */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $openapi = scan(__DIR__ . '/../../');
        return new JsonResponse(
            $openapi,
            200,
            [],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
        );
    }
}
