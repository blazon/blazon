<?php

declare(strict_types=1);

use Blazon\PSR11MonoLog\MonologFactory;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

return [
    'monolog' => [
        'handlers' => [
            'default' => [
                'type' => 'stream',
                'stream' => 'php://stdout',
            ],
        ],
    ],
    'dependencies' => [
        'aliases' => [
            LoggerInterface::class => Logger::class
        ],
        'factories' => [
            Logger::class => MonologFactory::class,
        ],
    ],
];
