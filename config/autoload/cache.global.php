<?php

declare(strict_types=1);

use Blazon\PSR11SymfonyCache\CacheFactory;

return [
    'cache' => [
        'default' => [
            'type' => 'array',
            'options' => []
        ],
    ],
    'dependencies' => [
        'factories' => [
            'cache' => CacheFactory::class
        ],
    ],
];
