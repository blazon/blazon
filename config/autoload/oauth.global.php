<?php

declare(strict_types=1);

return [
    'oauth2' => [
        'privateKeyPath'             => __DIR__ . '/../../data/keys/private.key',
        'publicKeyPath'              => __DIR__ . '/../../data/keys/public.key',
        'encryptionKeyPath'          => __DIR__ . '/../../data/keys//encryption.key',
        'accessTokenExpireInterval'  => 'P1D',   // 1 day in DateInterval format
        'refreshTokenExpireInterval' => 'P1M',   // 1 month in DateInterval format
        'authCodeExpireInterval'     => 'PT10M', // 10 minutes in DateInterval format
        'authenticationRouteName'     => 'auth',
    ],
];
