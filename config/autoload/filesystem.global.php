<?php

declare(strict_types=1);

use Blazon\PSR11FlySystem\FlySystemFactory;

return [
    'flysystem' => [
        'adaptor' => [
            'default' => [
                'type' => 'memory',
                'options' => []
            ],
        ],
    ],
    'dependencies' => [
        'factories' => [
            'filesystem' => FlySystemFactory::class
        ],
    ],
];
