FROM php:cli

RUN apt-get -y update \
  && apt-get -y install git unzip build-essential libzip-dev mariadb-client \
     libicu-dev libpq-dev

RUN docker-php-ext-install -j$(nproc) zip mysqli pdo_mysql intl pdo_pgsql

RUN pecl install xdebug

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN chmod -R 777 /root/.composer
